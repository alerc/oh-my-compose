# Oh My Composé

A collection of some useful [Composé][1] sequences.

[1]: https://en.wikipedia.org/wiki/Compose_key

```
$ git clone https://gitlab.com/alerc/oh-my-compose.git ~/.oh-my-compose
$ ~/.oh-my-compose/install.sh
```
