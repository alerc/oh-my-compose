#!/usr/bin/env bash

echo '   ____  __'
echo '  / __ \/ /_               ______                                  __'
echo ' / / / / __ \__  ___      / ____/___  ____ ___  ____  ____  ______/_/'
echo '/ /_/ / / / /  |/  /_  __/ /   / __ \/ __ `__ \/ __ \/ __ \/ ___/ _ \'
echo '\____/_/ /_/ /|_/ / / / / /___/ /_/ / / / / / / /_/ / /_/ (__  )  __/'
echo '          / /  / / /_/ /\____/\____/_/ /_/ /_/ .___/\____/____/\___/'
echo '         /_/  /_/\__, /                     /_/'
echo '                /____/'
echo '                          https://gitlab.com/alerc/oh-my-compose'
echo

if [[ -f "${HOME}/.XCompose" ]]; then
  echo "An .XCompose file already exists, would you like to override or append to it?"
  read -p "o/a>" choice
  case "${choice}" in 
    o|override)
      mv "${HOME}/.XCompose" "${HOME}/.XCompose.bak"
      echo "You can find your old .XCompose file at ~/.XCompose.bak."
      ;;
    a|append)
      # Add a newline in case it isn't there already
      echo >> "${HOME}/.XCompose"
      ;;
    *)
      echo "Aborting."
      exit 1
      ;;
  esac
fi

for category in arrows common currencies diacritics math music offtop; do
  read -p "Add ${category} (y/n)?" choice
  case "${choice}" in 
    y|Y)
      echo "include \"${HOME}/.oh-my-compose/${category}\"" >> "${HOME}/.XCompose"
      ;;
  esac
done
